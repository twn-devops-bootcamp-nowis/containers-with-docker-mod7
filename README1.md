# Containers with Docker ![Project Badge][badge-img-link]

![Docker Logo](Link to Docker project logo)

**Containers with Docker** is a practical exercise module designed to enhance skills in configuring and running applications with MySQL database using Docker and docker-compose. 

This module is part of a larger DevOps Bootcamp and includes a series of hands-on exercises to familiarize you with Docker containers.

![Overview of Docker Exercise](Link to project overview image)

## Exercises for Module "Containers with Docker"

Use repository: [https://gitlab.com/twn-devops-bootcamp/latest/07-docker/docker-exercises](https://gitlab.com/twn-devops-bootcamp/latest/07-docker/docker-exercises)

### EXERCISE 0: Clone Git Repository and Create Your Own

<details><summary><b>Show instructions</b></summary>

- Clone the Git repository and create a personal branch.
- Understand the importance of using environment variables for database credentials.

</details>

### EXERCISE 1: Start Mysql Container

<details><summary><b>Show instructions</b></summary>

- Start a MySQL container using the official Docker image.
- Set environment variables for application-database connection.
- Build a jar file and start the application.

</details>

### EXERCISE 2: Start Mysql GUI Container

<details><summary><b>Show instructions</b></summary>

- Deploy phpMyAdmin using the official Docker image.
- Access phpMyAdmin and log in to the MySQL database.

</details>

### EXERCISE 3: Use Docker-Compose for Mysql and Phpmyadmin

<details><summary><b>Show instructions</b></summary>

- Create a `docker-compose` file for MySQL and phpMyAdmin.
- Configure a volume for your database.

</details>

### EXERCISE 4: Dockerize Your Java Application

<details><summary><b>Show instructions</b></summary>

- Create a Dockerfile for your Java application.

</details>

### EXERCISE 5: Build and Push Java Application Docker Image

<details><summary><b>Show instructions</b></summary>

- Create a Docker hosted repository on Nexus.
- Build and push the image to this repository.

</details>

### EXERCISE 6: Add Application to Docker-Compose

<details><summary><b>Show instructions</b></summary>

- Add your Java application's Docker image to `docker-compose`.
- Configure all necessary environment variables.

</details>

### EXERCISE 7: Run Application on Server with Docker-Compose

<details><summary><b>Show instructions</b></summary>

- Prepare the server for running the application.
- Copy `docker-compose.yaml` to the server and set required environment variables.
- Start the application using `docker-compose`.

</details>

### EXERCISE 8: Open Ports

<details><summary><b>Show instructions</b></summary>

- Open necessary ports on the server firewall.
- Test application access from the browser.

</details>

### Exercise Solutions

Find example solutions [here](https://gitlab.com/twn-devops-bootcamp/latest/07-docker/docker-exercises/-/blob/feature/solutions/Solutions.md).

![Completed Docker Exercise Screenshot](Link to a screenshot showing a completed exercise)
